'use strict';

const gulp = require('gulp'),
    babel = require('gulp-babel'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rimraf = require('rimraf'),
    browserSync = require('browser-sync').create();

const path = {
    build: {
        html: '',
        js: 'assets/js/',
        css: 'assets/css/',
        img: 'assets/images/',
        fonts: 'assets/fonts/'
    },
    src: {
        html: 'src/*.html',
        js: 'src/js/app.js',
        style: 'src/styles/app.scss',
        img: 'src/images/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    watch: {
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        style: 'src/styles/**/*.scss',
        img: 'src/images/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    clean: './assets',
    node_modules: 'node_modules'
};

function swallowError (error) {

  // If you want details of the error in the console
  console.log(error.toString());

  this.emit('end');
}

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('html:build', function () {
    gulp.src(path.src.html) 
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html))
        .pipe(browserSync.stream());
});

gulp.task('js:build', function () {
    gulp.src(path.src.js)
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(rigger()) 
        .pipe(sourcemaps.init())
        .pipe(uglify()) 
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.js))
        .pipe(browserSync.stream());
});

gulp.task('styles:build', function () {
    gulp.src(path.src.style) 
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: ['src/styles/'],
            outputStyle: 'compressed',
            sourceMap: true,
            errLogToConsole: true
        }))
        .on('error', swallowError)
        .pipe(prefixer())
        .pipe(cssmin())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css))
        .pipe(browserSync.stream());
});

gulp.task('images:build', function () {
    gulp.src(path.src.img) 
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img))
        .pipe(browserSync.stream());
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

gulp.task('font-awesome-icons', function() {
    gulp.src(path.node_modules + '/font-awesome-sass/assets/fonts/font-awesome/**.*')
        .pipe(gulp.dest(path.build.fonts));
})

gulp.task('build', [
    'html:build',
    'js:build',
    'styles:build',
    'fonts:build',
    'images:build',
    'font-awesome-icons'
]);


gulp.task('watch', function() {

    browserSync.init({
        server: "./"
    });

    gulp.watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    gulp.watch(path.watch.html).on('change', browserSync.reload);
    gulp.watch([path.watch.style], function(event, cb) {
        gulp.start('styles:build');
    });
    gulp.watch(path.watch.style).on('change', browserSync.reload);
    gulp.watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    gulp.watch(path.watch.js).on('change', browserSync.reload);
    gulp.watch([path.watch.img], function(event, cb) {
        gulp.start('images:build');
    });
    gulp.watch(path.watch.img).on('change', browserSync.reload);
    gulp.watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
});


gulp.task('default', ['build', 'watch']);