//= ../../node_modules/jquery/dist/jquery.min.js
//= ../../node_modules/bootstrap/js/dist/util.js
//= ../../node_modules/bootstrap/js/dist/modal.js
//= ../../node_modules/select2/dist/js/select2.full.min.js
//= ../../node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js
//= ../../node_modules/magicsuggest/magicsuggest-min.js
//= ../../node_modules/jquery.filer/js/jquery.filer.js

;(function () {
	'use strict';

	$('.input-group.date').datepicker();

	if ($('#input-suggest-groups') != null) {
        $('#magic-suggest').magicSuggest({
            placeholder: 'Enter Group name',
            data: ['Alumni of America Group', 'American Designers Group', 'America Developers Group']
        });
    }
}());